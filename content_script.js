/**
 * Handle any information storage that should happen when we acquire focus of 
 * a Facebook page (for example when maximizing the browser after having 
 * minimized it). This mostly means updating the stored info of the last 
 * active page to be this page. We do not want to update the stored aggregate 
 * times here because we already handle that in the event page listener or 
 * in onLoseFocus()
 */
function onAcquireFocus() {
  currentTime = new Date().getTime() / 1000;
  var profileName = document.getElementById('fb-timeline-cover-name');
  var profilePic = document.getElementsByClassName('profilePic img');
  var lastPageInfo = {'timeOfActivation': currentTime, 'profileInfo': null};
  // If this page is not a profile the profileInfo will be null 
  if( profileName && profileName.innerHTML && profilePic && profilePic[0] && 
     profilePic[0].src)
  {
    lastPageInfo.profileInfo = {'name': profileName.innerHTML, 
	'pictureUrl': profilePic[0].src};
  }
  chrome.storage.sync.set({"lastPageInfo": lastPageInfo}, function(){});
}

/**
 * Handle any information storage that should happen when we lose focus of 
 * a Facebook page (for example by closing the browser). This mostly means 
 * updating our aggregate and by-profile times. We need to set our lastPageInfo 
 * to null so when we return to Facebook we do not add the time we were off 
 * Facebook to our time counts. I am choosing not to include comments within 
 * this function because updateTime in event_page.js is very similar and already 
 * has comments. Future work on this project would include adding message passing
 * mechanisms to save code, but for now, this is good enough 
 */
function onLoseFocus(){
  var valuesToGet = ['aggregateTime','profileTimes','lastPageInfo'];
  chrome.storage.sync.get(valuesToGet, function(currentValues) 
  {
	var currentTime = new Date().getTime() / 1000;
	var lastPageProfileInfo = null;
	if(currentValues.lastPageInfo)
	{
	  if(currentValues.lastPageInfo.timeOfActivation)
	  {
        var newAggregateTime = 0;
        if(currentValues.aggregateTime)
        {
          newAggregateTime = currentValues.aggregateTime;
	    }  
	    var pageActiveSeconds = currentTime - 
		                        currentValues.lastPageInfo.timeOfActivation;
	    chrome.storage.sync.set({"aggregateTime": newAggregateTime + 
		                         pageActiveSeconds}, function(){});
		if(currentValues.lastPageInfo.profileInfo)
		{
		  lastPageProfileInfo = currentValues.lastPageInfo.profileInfo;
	      var newProfileTimes = {}; 
          if(currentValues.profileTimes)
          {
            newProfileTimes = currentValues.profileTimes;
	      }
		  if(newProfileTimes[lastPageProfileInfo.name])
	      {
            newProfileTimes[lastPageProfileInfo.name].time += pageActiveSeconds;
 	      }
	      else
	      {
            newProfileTimes[lastPageProfileInfo.name] = {"time" : pageActiveSeconds, 
			"pictureUrl" : lastPageProfileInfo.pictureUrl};  
	      }
	      chrome.storage.sync.set({"profileTimes": newProfileTimes}, 
		                           function(){});
        }			  
	  }
	}
	// Based on the event handlers that trigger this, we know we are 
	// not navigating to another Facebook page, so we indicate this 
	// by setting lastPageInfo to null 
	chrome.storage.sync.set({"lastPageInfo": null}, function(){});	
  });
}

// We want to update lastPageInfo anytime the content script loads
// on Facebook
onAcquireFocus();

// Handles such browser events as minimizing/maximizing Chrome and 
// changing tabs 
document.addEventListener("visibilitychange", function() {
  if(document.hidden)
  {
    onLoseFocus();	
  }
  else
  {
    onAcquireFocus();
  }
});

// Handles such browser events as closing Chrome or closing a tab
window.addEventListener("beforeunload", function (e) {
  onLoseFocus();
});