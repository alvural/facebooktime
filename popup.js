/**
 * I internally store the time counts as seconds, but we want them displayed 
 * as a string of hours/minutes/seconds, and so this function handles that 
 * conversion 
 *
 * @param {int} seconds - the amount of seconds to convert to h/m/s 
 */
function formatSeconds(seconds)
{
  var hours = Math.floor(seconds / 3600);
  seconds -= hours * 3600;
  var minutes = Math.floor(seconds / 60);
  seconds -= minutes * 60;
  return hours.toString() + "h, " + minutes.toString() + "m, " + 
         Math.floor(seconds).toString() +"s";
}

// Display our aggregate and by-profile times in the popup. We also need to 
// account for the time the user was on the page before they clicked the popup
// that portion of the code is extremely similar to updateTime in event_page.js
// and so my comments will mostly be focused on anything that is different from 
// there.  
document.addEventListener('DOMContentLoaded', function() {
  var valuesToGet = ['aggregateTime','profileTimes','lastPageInfo'];
  chrome.storage.sync.get(valuesToGet, function(currentValues) 
  {
	var currentTime = new Date().getTime() / 1000;
	var lastPageProfileInfo = null;
	var newAggregateTime = 0;
    if(currentValues.aggregateTime)
    {
      newAggregateTime = currentValues.aggregateTime;
	}  
	var newProfileTimes = {}; 
    if(currentValues.profileTimes)
    {
      newProfileTimes = currentValues.profileTimes;
	}
	// Having a null lastPageInfo would mean we are opening the popup
	// from a non-Facebook page since no update is needed in that case 
	if(currentValues.lastPageInfo)
	{
	  if(currentValues.lastPageInfo.timeOfActivation)
	  {
		// We don't need to update any of our stored counts right now 
		// since we can still do that when the page changes 
		var pageActiveSeconds = currentTime - 
		                        currentValues.lastPageInfo.timeOfActivation;
	    newAggregateTime += pageActiveSeconds;
		if(currentValues.lastPageInfo.profileInfo)
		{
		  lastPageProfileInfo = currentValues.lastPageInfo.profileInfo;
		  if(newProfileTimes[lastPageProfileInfo.name])
	      {
            newProfileTimes[lastPageProfileInfo.name].time += pageActiveSeconds;
 	      }
	      else
	      {
            newProfileTimes[lastPageProfileInfo.name] = {"time" : pageActiveSeconds, 
			"pictureUrl" : lastPageProfileInfo.pictureUrl};  
	      }
        }			  
	  }
	}
	// Set the DOM element showing the aggregate Facebook time	
    var aggregateTimeDOM = document.getElementById('aggregateTime'); 
	aggregateTimeDOM.innerHTML = "Total Facebook time today:<br />" + 
	                             formatSeconds(newAggregateTime);
	profileTimes = document.getElementById('profileTimes'); 
	contentToInsert = "Facebook time today by profile:<br />";
	var oneOrMore = false;
	// Get the infromation for our by-profile times 
	for( personName in newProfileTimes)
	{
	  oneOrMore = true;
	  contentToInsert += personName + ": ";
	  contentToInsert += formatSeconds(newProfileTimes[personName].time) + "<br />";
	  contentToInsert += "<img src=" + newProfileTimes[personName].pictureUrl + 
	                     " width=100 height=100>";
	  contentToInsert += "<br />";
	}
	// Only need to update the DOM element showing by-profile time if 
	// we have visited one or more profiles 
	if(oneOrMore)
	{
	  profileTimes.innerHTML = contentToInsert;
	}
  });
});