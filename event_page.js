// There are likely betters ways to handle the reset-on-next-day mechanism but
// this was sufficient in my opinion for the purposes of this project.
// dayOfEntries represents the day of the month for our currently stored values 		
chrome.storage.sync.get("dayOfEntries", function(currentValue){
	var date = new Date();
    var currentDay = date.getDate();
    // If our current day is not equal to the day we have on record, that means
    // our stored information corresponds to another day and should be cleared.
    if(currentValue.dayOfEntries && currentDay != currentValue.dayOfEntries)
    {		
      chrome.storage.sync.set({"aggregateTime": null, "profileTimes": null, 
	                           "lastPageInfo": null}, function() {});
	}
    // Update the day tracked in our records 
    chrome.storage.sync.set({"dayOfEntries": currentDay}, function(){});
});	

/**
 * Update our stored values for Facebook times. We need to update our total 
 * time we have been on Facebook (aggregateTime), our times for each profile 
 * (profileTimes), and data about what page was most recently activated 
 * (lastPageInfo). Note that if the most recently activated page is not a
 * Facebook page, we will store its information as null in order to not 
 * increment any of the time counters next time we update our time 
 *
 * @param {boolean} newUrlIsFb - Indicates if the Url we will be on is Facebook
 * or not
 */
function updateTime(newUrlIsFb) {
  // Retrieve our current values
  var valuesToGet = ['aggregateTime','profileTimes','lastPageInfo'];
  chrome.storage.sync.get(valuesToGet, function(currentValues) 
  {
	var currentTime = new Date().getTime() / 1000;
    // If the most recently active page was a profile, we will retrieve its 
    // data later in the function; otherwise this will stay null.
	var lastPageProfileInfo = null;
    // We can only update our times if we know how long we were on this page, 
    // and for that we need data about when the page was loaded 
	if(currentValues.lastPageInfo)
	{
	  if(currentValues.lastPageInfo.timeOfActivation)
	  {
        // Start "aggregate Facebook time" at 0 if no value already
        // Otherwise start from the existing value 
        var newAggregateTime = 0;
        if(currentValues.aggregateTime)
        {
          newAggregateTime = currentValues.aggregateTime;
	    }  
	    // Add time this page was active to our aggregate total and update
	    // stored value
	    var pageActiveSeconds = currentTime - 
		                        currentValues.lastPageInfo.timeOfActivation;
	    chrome.storage.sync.set({"aggregateTime": newAggregateTime + 
		                         pageActiveSeconds}, function(){});
        // If the last page was a profile, update the time for that profile in
		// our by-profile info
		if(currentValues.lastPageInfo.profileInfo)
		{
		  lastPageProfileInfo = currentValues.lastPageInfo.profileInfo;
          // If we already have visited a profile today, we use that
          // information  but otherwise we start with an empty dictionary
	      var newProfileTimes = {}; 
          if(currentValues.profileTimes)
          {
            newProfileTimes = currentValues.profileTimes;
	      }
	      // If we already have data for this profile, add the time we were on 
		  // this page to the previous count; otherwise create a new entry for
       	  // this profile; In both cases, update our stored record 
		  if(newProfileTimes[lastPageProfileInfo.name])
	      {
            newProfileTimes[lastPageProfileInfo.name].time += pageActiveSeconds;
 	      }
	      else
	      {
            newProfileTimes[lastPageProfileInfo.name] = {"time" : pageActiveSeconds, 
			"pictureUrl" : lastPageProfileInfo.pictureUrl};  
	      }
	      chrome.storage.sync.set({"profileTimes": newProfileTimes}, 
		                           function(){});
        }			  
	  }
	}
	// If we are not on Facebook, set the information to be null so that
	// when we return to Facebook we do not use the amount of time we were 
	// on the non-Facebook page to update our time counts 
	if(newUrlIsFb)
	{
	  chrome.tabs.executeScript(null, {file: "content_script.js"});
	}
	else
	{
	  chrome.storage.sync.set({"lastPageInfo": null}, function(){});	  
	}
  });
}

// Handler for when the page is completely finished loading. This is used for 
// 1. Detecting url changes such as when user navigates to another page within 
//    FB
// 2. Injecting content script at the appropriate time - the window.onbeforeload
//    function is still too soon for some Facebook images to finish loading, and 
//    we need to do it after we have the data from the last page to update our 
//    our time counters or else it will be overridden before we can use it  
chrome.webNavigation.onCompleted.addListener(function(details) {
  var facebookUrl = "https://www.facebook.com/";
  var isFbUrl = details.url.substring(0, facebookUrl.length) === 
	            facebookUrl;
  updateTime(isFbUrl);
});